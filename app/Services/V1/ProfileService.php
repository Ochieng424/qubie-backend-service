<?php

namespace App\Services\V1;

use App\Models\Location;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileService
{
    public function newLocation($longitude, $latitude, $userId)
    {

        $location = new Point($latitude, $longitude);
        $newLocation = new Location();
        $newLocation->user_id = $userId;
        $newLocation->location = $location;
        $newLocation->save();
    }

    public function updateProfile(Request $request)
    {
        DB::transaction(function () use ($request) {
            $user = $request->user();
            $user->name = $request->name;
            $user->save();

            $user->profile()->update([
                'about' => $request->about,
                'profession' => $request->profession,
            ]);
        });
    }

    public function updatePreferences(Request $request)
    {
        $user = $request->user();
        $user->preference()->update([
            'age_range' => $request->age_range,
            'gender' => $request->gender,
            'distance' => $request->distance,
        ]);
    }

    public function manageInterests(Request $request){
        $user = $request->user();
        // convert array to string
        $interests = implode('#', $request->interests);
        $user->profile()->update([
            'interests' => $interests,
        ]);
    }
}
