<?php

namespace App\Services\V1;

use \Softon\LaravelFaceDetect\Facades\FaceDetect;

class ImageService
{

    public function convertImageFromBase64($base64)
    {
        $image = str_replace('data:image/png;base64,', '', $base64);
        $image = str_replace(' ', '+', $image);

        return base64_decode($image);
    }

    public function imageHasFace($image)
    {
        return FaceDetect::extract($image)->face_found;
    }
}
