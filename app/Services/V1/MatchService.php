<?php

namespace App\Services\V1;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MatchService
{
  public function nearBySingles(Request $request){
      $matchQuery = (new User())->newQuery();
      $user = $request->user();
      $dist = 5000;
      $point = $user->location->location;
      $unit = 'km';

      $matchQuery->where('is_active', true);
      $matchQuery->where('is_staff', false);
      $matchQuery->whereHas('location', function (Builder $query) use ($point, $dist) {
          $query->distanceSphereExcludingSelf('location', $point, $dist);
      });

      return $matchQuery->get();
  }
}
