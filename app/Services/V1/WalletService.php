<?php

namespace App\Services\V1;

class WalletService
{
    public function loadWallet($user, $amount)
    {
        $user->deposit($amount);

        return $user->balance;
    }

    public function withdrawWallet($user, $amount)
    {
        $user->withdraw($amount);

        return $user->balance;
    }

    public function transferWallet($user, $amount, $to)
    {
        $user->transfer($to, $amount);

        return $user->balance;
    }
}
