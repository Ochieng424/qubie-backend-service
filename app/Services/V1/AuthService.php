<?php

namespace App\Services\V1;

use App\Models\MatchPreference;
use App\Models\Photo;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class AuthService
{
    public function register(Request $request)
    {
        DB::transaction(function () use ($request) {
            $user = $this->createUser($request);
            $this->createProfile($request, $user);
            $this->createUserImage($request, $user);
            $this->createMatchPreference($request, $user);
        });
    }

    private function createUser(Request $request): User
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->status = 'waitlist';
        $user->password = Hash::make($request->password);
        $user->save();

        return $user;
    }

    private function createProfile(Request $request, User $user)
    {
        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->gender = $request->gender;
        $profile->dob = $request->dob;
        $profile->social_link = $request->socialLink;
        $profile->save();
    }

    private function createUserImage(Request $request, $user)
    {
        $imageService = new ImageService();
        $image = $imageService->convertImageFromBase64($request->image);
        $imageName = time() . '.' . 'png';
        Storage::put('public/profile/' . $imageName, $image);

        $userPhoto = new Photo();
        $userPhoto->user_id = $user->id;
        $userPhoto->path = $imageName;
        $userPhoto->save();
    }

    private function createMatchPreference(Request $request, $user)
    {
        $matchPreference = new MatchPreference();
        $matchPreference->user_id = $user->id;
        $matchPreference->age_range = $request->ageRange;
        $matchPreference->gender = $request->preferenceGender;
        $matchPreference->save();
    }

    public function checkIfEmailTaken($email): bool
    {
        $email = User::where('email', $email)->first();

        if ($email) {
            return true;
        }
        return false;
    }

    public function login(Request $request): array
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

//        if (!$user->is_active) {
//            throw ValidationException::withMessages([
//                'email' => ['Your account has been suspended'],
//            ]);
//        }

        if (!Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

//        if (!$user->hasVerifiedEmail() && $user->role == 'user') {
//            $this->authService->sendVerificationCode($user);
//        }

        return [
            'success' => true,
            'token' => $user->createToken($request->device_name)->plainTextToken,
//            'verified' => $user->hasVerifiedEmail(),
        ];
    }

    public function loginDashboard(Request $request): array
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !$user->is_staff) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        if (!Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return [
            'success' => true,
            'token' => $user->createToken('dashboard')->plainTextToken,
        ];
    }

    public function me(Request $request){
        $user = $request->user();
        return $user;
    }

    public function refresh(Request $request): array
    {
        $user = $request->user();
        $user->tokens()->delete();
        $token = $user->createToken('dashboard')->plainTextToken;
        return [
            'success' => true,
            'token' => $token,
        ];
    }

    public function logout(Request $request){
        $user = $request->user();
        return $user->tokens()->delete();
    }

    /**
     * @throws ValidationException
     */
    public function changePassword(Request $request){
        $user = $request->user();
        if (!Hash::check($request->old_password, $user->password)) {
            throw ValidationException::withMessages([
                'old_password' => ['The old password is incorrect.'],
            ]);
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return $user;
    }
}
