<?php

namespace App\Services\V1;

use App\Models\Plan;

class PlanService
{
    public function getAll(){
        return Plan::orderBy('days_number', 'desc')->get();
    }
   public function create($request): Plan
   {
      $plan = new Plan();
       $this->handlePlan($request, $plan);
       $plan->unique_id = strtoupper($request->name) . '-' . time();
      $plan->save();

      return $plan;
   }

   public function update($request, $id)
   {
      $plan = Plan::find($id);
       $this->handlePlan($request, $plan);
       $plan->save();

      return $plan;
   }

    /**
     * @param $request
     * @param $plan
     * @return void
     */
    public function handlePlan($request, $plan): void
    {
        $plan->name = $request->name;
        $plan->price = $request->price;
        $plan->color = $request->color;
        $plan->description = $request->description;
        $plan->days_number = $request->daysNumber;
        $plan->duration = $request->duration;
        $plan->coins = $request->coins;
    }
}
