<?php

namespace App\Services\V1;

use App\Models\Payment;
use App\Models\Plan;
use App\Models\Subscription;
use Illuminate\Support\Facades\Http;

class SubscriptionService
{
    public function subscribeToPremium($planId, $userId)
    {
        $plan = Plan::find($planId);
        $duration = $plan->days_number;
        $endOfSubscription = now()->addDays($duration);

        $subscription = new Subscription();
        $subscription->user_id = $userId;
        $subscription->plan_id = $planId;
        $subscription->end_of_subscription = $endOfSubscription;
        $subscription->save();
    }

    public function payForPlan($planId, $user, $phone): Payment
    {
        $plan = Plan::find($planId);
        $amount = $plan->price;
        $fw_url = env('FW_URL');
        $data = [
            "tx_ref" => $plan->name . '-' . $user->id . '-' . now()->timestamp,
            "amount" => $amount,
            "currency" => "KES",
            "email" => $user->email,
            "phone_number" => $phone,
            "fullname" => $user->name,
        ];

        $response = Http::acceptJson()->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . env('FW_SECRET_KEY'),
        ])->post($fw_url, $data);

        return $this->createPaymentRecord($response, $user, $plan->id);
    }

    public function createPaymentRecord($response, $user, $planId): Payment
    {
        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->plan_id = $planId;
        $payment->flw_id = $response['data']['id'];
        $payment->flw_ref = $response['data']['flw_ref'];
        $payment->amount = $response['data']['amount'];
        $payment->currency = $response['data']['currency'];
        $payment->tx_ref = $response['data']['tx_ref'];
        $payment->status = $response['data']['status'];
        $payment->charged_amount = $response['data']['charged_amount'];
        $payment->app_fee = $response['data']['app_fee'];
        $payment->merchant_fee = $response['data']['merchant_fee'];
        $payment->payment_type = $response['data']['payment_type'];
        $payment->ip_address = $response['data']['ip'];
        $payment->phone_number = $response['data']['customer']['phone_number'];
        $payment->save();

        return $payment;
    }
}
