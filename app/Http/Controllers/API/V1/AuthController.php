<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Services\V1\ImageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\V1\AuthService;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'image' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'gender' => 'required|string',
            'dob' => 'required|date',
            'ageRange' => 'required|string',
            'preferenceGender' => 'required|string',
            'socialLink' => 'required|url',
        ]);

//        $imageService = new ImageService();
//        $image = $imageService->convertImageFromBase64($request->image);
//        $hasFace = $imageService->imageHasFace($image);
//
//        if (!$hasFace){
//            throw ValidationException::withMessages([
//                'image' => ['Invalid Image.'],
//            ]);
//        }

        $this->authService->register($request);

        return response()->json([
            'message' => 'User created successfully'
        ], 201);
    }

    public function checkIfEmailTaken(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
        ]);

        $emailTaken = $this->authService->checkIfEmailTaken($request->email);

        return response()->json([
            'message' => 'Email is available',
            'emailTaken' => $emailTaken
        ], 200);
    }

    /**
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required',
        ]);

        $data = $this->authService->login($request);

        return response()->json($data);
    }

    /**
     * @throws ValidationException
     */
    public function loginDashboard(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $data = $this->authService->loginDashboard($request);

        return response()->json($data)->header('Authorization', $data['token']);
    }

    public function me(Request $request): JsonResponse
    {
        $data = $this->authService->me($request);

        return response()->json($data);
    }

    public function refresh(Request $request): JsonResponse
    {
        $data = $this->authService->refresh($request);
        return response()->json($data)->header('Authorization', $data['token']);
    }

    public function logout(Request $request): JsonResponse
    {
        $data = $this->authService->logout($request);
        return response()->json($data);
    }

    /**
     * @throws ValidationException
     */
    public function changePassword(Request $request): JsonResponse
    {
        $request->validate([
            'old_password' => 'required',
            'password' => 'required|min:8',
        ]);

        $data = $this->authService->changePassword($request);

        return response()->json($data);
    }
}
