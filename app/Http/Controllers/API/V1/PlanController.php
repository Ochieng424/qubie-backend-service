<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\V1\PlanService;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class PlanController extends Controller
{
    protected $planService;

    public function __construct(PlanService $planService)
    {
        $this->planService = $planService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $plans = $this->planService->getAll();

        return response()->json($plans);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|string',
            'price' => 'required|numeric',
            'color' => 'required|string',
            'description' => 'required|string',
            'duration' => 'required|string',
            'coins' => 'required|numeric',
            'daysNumber' => 'required|numeric',
        ]);

        $plan = $this->planService->create($request);

        return response()->json($plan, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|string',
            'price' => 'required|numeric',
            'color' => 'required|string',
            'description' => 'required|string',
            'duration' => 'required|string',
            'coins' => 'required|numeric',
            'daysNumber' => 'required|numeric',
        ]);

        $plan = $this->planService->update($request, $id);

        return response()->json($plan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
