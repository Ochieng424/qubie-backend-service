<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\V1\ProfileService;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{
    protected $profileService;

    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function updateProfile(Request $request): JsonResponse
    {
        $this->validate($request, [
            'name' => 'required|string|max:25',
            'profession' => 'required|string|max:25',
            'about' => 'required|string',
        ]);

        $this->profileService->updateProfile($request);

        return response()->json(['message' => 'Profile updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        //
    }

    public function newLocation(Request $request): JsonResponse
    {
        $request->validate([
            'latitude' => 'required|numeric|min:-90|max:90',
            'longitude' => 'required|numeric|min:-180|max:180',
        ]);

        $userId = $request->user()->id;
        $this->profileService->newLocation($request->longitude, $request->latitude, $userId);

        return response()->json([
            'message' => 'Location updated successfully'
        ], 200);
    }

    public function updatePreferences(Request $request): JsonResponse
    {
        $request->validate([
            'age_range' => 'required|string|max:25',
            'distance' => 'required|numeric|min:1|max:100',
            'gender' => 'required|string'
        ]);
        $this->profileService->updatePreferences($request);

        return response()->json([
            'message' => 'Preferences updated successfully'
        ], 200);
    }

    public function manageInterests(Request $request): JsonResponse
    {
        $request->validate([
            'interests' => 'required|array',
            'interests.*' => 'required|string|max:25',
        ]);
        $this->profileService->manageInterests($request);

        return response()->json([
            'message' => 'Interests updated successfully'
        ], 200);
    }
}
