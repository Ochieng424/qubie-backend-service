<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\AuthController;
use App\Http\Controllers\API\V1\ProfileController;
use App\Http\Controllers\API\V1\PlanController;
use App\Http\Controllers\API\V1\SubscriptionController;
use App\Http\Controllers\API\V1\MatchController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');;
        Route::get('user', [AuthController::class, 'me'])->middleware('auth:sanctum');
        Route::get('refresh', [AuthController::class, 'refresh'])->middleware('auth:sanctum');
    });

    Route::post('register', [AuthController::class, 'register']);
    Route::post('email-taken', [AuthController::class, 'checkIfEmailTaken']);

    Route::prefix('dashboard')->middleware(['auth:sanctum'])->group(function () {
        Route::post('login', [AuthController::class, 'loginDashboard'])->withoutMiddleware('auth:sanctum');
        Route::resource('plans', PlanController::class);
    });

    Route::prefix('settings')->middleware(['auth:sanctum'])->group(function () {
        Route::post('new-location', [ProfileController::class, 'newLocation']);
        Route::resource('subscription', SubscriptionController::class);
        Route::patch('profile/update', [ProfileController::class, 'updateProfile']);
        Route::post('profile/interests', [ProfileController::class, 'manageInterests']);
        Route::post('password/change', [AuthController::class, 'changePassword']);
        Route::patch('preference/update', [ProfileController::class, 'updatePreferences']);
    });

    Route::resource('match', MatchController::class)->middleware('auth:sanctum');
});
