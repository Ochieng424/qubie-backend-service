<?php

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('flw_id');
            $table->string('flw_ref');
            $table->string('tx_ref');
            $table->decimal('amount', 10, 2);
            $table->decimal('charged_amount', 10, 2);
            $table->string('currency');
            $table->decimal('app_fee', 10, 2);
            $table->string('merchant_fee');
            $table->string('status');
            $table->string('payment_type');
            $table->string('ip_address');
            $table->string('phone_number');
            $table->foreignIdFor(User::class)->constrained();
            $table->foreignIdFor(Subscription::class)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
